package clases;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by PROGRA on 04/07/2017.
 */
public class Ticket {
    private int ticketId;
    private String memorandum;
    private String descripcion;
    private List<Iteracion> listadoIteraciones;
    private List<Prueba> listadoPruebas;

    public List<Prueba> getListadoPruebas() {
        return listadoPruebas;
    }

    public void setListadoPruebas(List<Prueba> listadoPruebas) {
        this.listadoPruebas = listadoPruebas;
    }

    public int getTicketId() {
        return ticketId;
    }

    public void setTicketId(int ticketId) {
        this.ticketId = ticketId;
    }

    public String getMemorandum() {
        return memorandum;
    }

    public void setMemorandum(String memorandum) {
        this.memorandum = memorandum;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<Iteracion> getListadoIteraciones() {
        return listadoIteraciones;
    }

    public void setListadoIteraciones(List<Iteracion> listadoIteraciones) {
        this.listadoIteraciones = listadoIteraciones;
    }

    public Ticket() {
        listadoPruebas = new ArrayList<>();
        listadoIteraciones = new ArrayList<>();
    }

    public Ticket(int ticketId, String memorandum, String descripcion, List<Iteracion> listadoIteraciones) {
        this.ticketId = ticketId;
        this.memorandum = memorandum;
        this.descripcion = descripcion;
        this.listadoIteraciones = listadoIteraciones;
    }

    public void AgregarIteraccion(Iteracion iteracion){
        listadoIteraciones.add(iteracion);
    }

    public void AgregarPrueba(Prueba prueba){
        listadoPruebas.add(prueba);
    }
}
