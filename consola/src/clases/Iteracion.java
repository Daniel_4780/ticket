package clases;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by PROGRA on 04/07/2017.
 */
public class Iteracion {
    private String AnalisitaQA;
    private String AnalistaDesa;
    private String AnalistaPDI;
    private String AnalistaNormativo;

    public String getAnalisitaQA() {
        return AnalisitaQA;
    }

    public void setAnalisitaQA(String analisitaQA) {
        AnalisitaQA = analisitaQA;
    }

    public String getAnalistaDesa() {
        return AnalistaDesa;
    }

    public void setAnalistaDesa(String analistaDesa) {
        AnalistaDesa = analistaDesa;
    }

    public String getAnalistaPDI() {
        return AnalistaPDI;
    }

    public void setAnalistaPDI(String analistaPDI) {
        AnalistaPDI = analistaPDI;
    }

    public String getAnalistaNormativo() {
        return AnalistaNormativo;
    }

    public void setAnalistaNormativo(String analistaNormativo) {
        AnalistaNormativo = analistaNormativo;
    }

    public Iteracion(){
    }

    public Iteracion(String analisitaQA, String analistaDesa, String analistaPDI, String analistaNormativo) {
        AnalisitaQA = analisitaQA;
        AnalistaDesa = analistaDesa;
        AnalistaPDI = analistaPDI;
        AnalistaNormativo = analistaNormativo;
    }
}
