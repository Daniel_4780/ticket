package principal;

import clases.*;

import javax.xml.crypto.KeySelector;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Grupo UMG on 06/07/2017.
 */
public class CargarDatos {
    private List<Ticket> listadoTicke;
    private List<Requerimiento> listadoRequerimiento;

    public List<Ticket> getListadoTicke() {
        return listadoTicke;
    }

    public void setListadoTicke(List<Ticket> listadoTicke) {
        this.listadoTicke = listadoTicke;
    }

    public List<Requerimiento> getListadoRequerimiento() {
        return listadoRequerimiento;
    }

    public void setListadoRequerimiento(List<Requerimiento> listadoRequerimiento) {
        this.listadoRequerimiento = listadoRequerimiento;
    }

    public CargarDatos() {
        CargarRequerimiento();
        CargarTicket();
    }

    public CargarDatos(List<Ticket> listadoTicke, List<Requerimiento> listadoRequerimiento) {
        this.listadoTicke = listadoTicke;
        this.listadoRequerimiento = listadoRequerimiento;
    }

    public void CargarTicket(){
        listadoTicke = new ArrayList<>();
        Iteracion iteracion = new Iteracion("DANIEL", "DANIEL", "DANIEL", "DANIEL");

        Ticket ticket = new Ticket();
        ticket.setTicketId(123);
        ticket.setMemorandum("GUATE-01");
        ticket.setDescripcion("FIRMA");
        ticket.AgregarIteraccion(iteracion);
        ticket.AgregarIteraccion(iteracion);
        ticket.AgregarIteraccion(iteracion);

        Prueba prueba = new Prueba("Fallido", 0,0,0,0);
        ticket.AgregarPrueba(prueba);
        prueba = new Prueba("Fallido", 0,1,0,1);
        ticket.AgregarPrueba(prueba);
        prueba = new Prueba("Exitoso", 0,2,0,2);
        ticket.AgregarPrueba(prueba);

        listadoTicke.add(ticket);

        ticket = new Ticket();
        ticket.setTicketId(456);
        ticket.setMemorandum("GUATE-02");
        ticket.setDescripcion("FIRMA");
        ticket.AgregarIteraccion(iteracion);
        ticket.AgregarIteraccion(iteracion);
        ticket.AgregarIteraccion(iteracion);

        prueba = new Prueba("Fallido", 1,1,1,0);
        ticket.AgregarPrueba(prueba);
        prueba = new Prueba("Fallido", 1,1,1,1);
        ticket.AgregarPrueba(prueba);
        prueba = new Prueba("Exitoso", 1,1,1,2);
        ticket.AgregarPrueba(prueba);

        listadoTicke.add(ticket);
    }

    public void CargarRequerimiento(){
        listadoRequerimiento = new ArrayList<>();
        List<Escenario> listaEscenario = new ArrayList<>();

        Escenario escenario = new Escenario("Numeros Romanos", "Sadisfactorio");
        listaEscenario.add(escenario);

        escenario = new Escenario("Numeros Romanos", "Sin Probar");
        listaEscenario.add(escenario);

        escenario = new Escenario("Numeros Romanos", "Fallido");
        listaEscenario.add(escenario);

        Requerimiento requerimiento = new Requerimiento("Regla_01", "1.1", listaEscenario);
        listadoRequerimiento.add(requerimiento);

        requerimiento = new Requerimiento("Regla_02", "1.2", listaEscenario);
        listadoRequerimiento.add(requerimiento);
    }


}
