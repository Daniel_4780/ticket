package principal;
import clases.*;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by PROGRA on 04/07/2017.
 */
public class consolaAppTicket {

    public void CargarDatos(){

    }

    public static void main(String[] args) {
        Scanner sn = new Scanner(System.in);
        boolean salir = true;
        int opcion;
        Sistema sistema = null;
        while(salir)
        {
            System.out.println("0. Cargar datos de prueba");
            System.out.println("1. Ingresar ticket");
            System.out.println("2. Ingresar iteracion");
            System.out.println("3. Ingresar requerimiento");
            System.out.println("4. Ingresar prueba");
            System.out.println("5. Mostar todos los ticket");
            System.out.println("6. Mostrar todos los requerimientos");
            System.out.println("7. Mostrar pruebas e iteraciones de todos los ticket.");
            System.out.println("8. Salir");
            try{
                if(sistema == null){
                    sistema = new Sistema();
                }

                System.out.println("Seleccione una de las opciones");
                opcion = sn.nextInt();

                switch (opcion){
                    case 0:
                        CargarDatos cargarDatos = new CargarDatos();
                        sistema.setListadoRequerimientos(cargarDatos.getListadoRequerimiento());
                        sistema.setListadoTicket(cargarDatos.getListadoTicke());
                        break;
                    case 1:
                        Ticket ticket = new Ticket();
                        System.out.println("Ingrese el numero del ticket");
                        ticket.setTicketId(sn.nextInt());
                        System.out.println("Ingrese memorandum del ticket");
                        ticket.setMemorandum(sn.next());
                        System.out.println("Ingrse descripcion del ticket");
                        ticket.setDescripcion(sn.next());
                        System.out.println("Se ingreso correctamente el ticket: " + ticket.getTicketId() + "con memorandum: "+ ticket.getMemorandum());
                        sistema.AgregarTicket(ticket);
                        System.out.println("--------------");
                        break;
                    case 2:
                        Iteracion iteracion = new Iteracion();
                        System.out.println("Selecione un ticket");
                        sistema.MostrarTickets();
                        int indiceTicketIteracion = sn.nextInt();
                        System.out.println("Ingrese nombre del analisis QA");
                        iteracion.setAnalisitaQA(sn.next());
                        System.out.println("Ingrese nombre del analista Desarrollo");
                        iteracion.setAnalistaDesa(sn.next());
                        System.out.println("Ingrese nombre del analista normativo");
                        iteracion.setAnalistaNormativo(sn.next());
                        int numeroTicket = sistema.AgregarIteraccionTicket(indiceTicketIteracion, iteracion);
                        System.out.println("Se asigno correctamente la iteracion al ticket: " + numeroTicket);
                        System.out.println("--------------");
                        break;
                    case 3:
                        Requerimiento requerimiento = new Requerimiento();
                        System.out.println("Ingrese nombre del requerimiento");
                        requerimiento.setNombreRquerimiento(sn.next());
                        System.out.println("Ingrse la version del requerimiento");
                        requerimiento.setVersion(sn.next());
                        System.out.println("El requerimiento: " + requerimiento.getNombreRquerimiento() + " y version: " + requerimiento.getVersion() + ", fueron ingresados correctamente'");
                        System.out.println("Desea ingresar un escenario s/n");
                        while (sn.next().equalsIgnoreCase("S")){
                            Escenario escenario = new Escenario();
                            System.out.println("Ingrese descripcion del escenario");
                            escenario.setDescripcion(sn.next());
                            System.out.println("Ingrese el resultado esperado del escenario");
                            escenario.setResultadoEsperado(sn.next());
                            System.out.println("Se ingreso correctamente el escenario: " + escenario.getDescripcion());
                            requerimiento.AgregarEscenario(escenario);
                            System.out.println("Desea ingresar un escenario s/n");
                        }
                        sistema.AgregarRequerimiento(requerimiento);
                        System.out.println("--------------");
                        break;
                    case 4:
                        Prueba prueba = new Prueba();
                        System.out.println("Seleccione un ticket");
                        sistema.MostrarTickets();
                        prueba.setIndiceTicket(sn.nextInt()-1);
                        System.out.println("Seleccione una iteracion");
                        sistema.MostrarIteraciones(prueba.getIndiceTicket());
                        prueba.setIndiceIteracion(sn.nextInt()-1);
                        System.out.println("Seleccione un requerimiento");
                        sistema.MostrarRequerimientos();
                        prueba.setIndiceRequerimiento(sn.nextInt()-1);
                        System.out.println("Seleccione un escenario");
                        sistema.MostrarEscenarios(prueba.getIndiceRequerimiento());
                        prueba.setIndiceEscenario(sn.nextInt()-1);
                        System.out.println("Ingrese resultado");
                        prueba.setResultado(sn.next());
                        sistema.AgregarPruebaTicket(prueba);
                        System.out.println("La prueba fue asignada correctamente");
                        System.out.println("--------------");
                        break;
                    case 5:
                        sistema.MostrarTicketIteracion();
                        System.out.println("--------------");
                        break;
                    case 6:
                        sistema.MostrarRequerimientosEscenario();
                        System.out.println("--------------");
                        break;
                    case 7:
                        sistema.MostrarTodasPruebas();
                        System.out.println("--------------");
                        break;
                    case 8:
                        salir = false;
                        break;
                    default:
                        System.out.println("Solo números entre 0 y 8");
                }
            }
            catch (InputMismatchException e) {
                System.out.println("Debes insertar un número");
                sn.next();
            }
        }
    }
}
