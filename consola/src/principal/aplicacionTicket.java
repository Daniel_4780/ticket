package principal;

import clases.*;
import sun.rmi.server.InactiveGroupException;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.util.List;

public class aplicacionTicket extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTabbedPane tabbedPane1;
    private JTextField txtTicketId;
    private JTextField txtMemorandun;
    private JTextField txtDescripcion;
    private JButton btnCrearTicket;
    private JTextField txtAnalisitaDessa;
    private JTextField txtAnlistaQA;
    private JTextField txtAnliastaDPI;
    private JTextField txtAnlistaNorma;
    private JButton btnCrearIteracion;
    private JTextField txtNombreRequerimiento;
    private JTextField txtVersion;
    private JTextField txtDescripcionEscenario;
    private JTextField txtResultadoEscenario;
    private JButton btnAgregarEscenario;
    private JTable tbEscenarios;
    private JButton crearRequerimientoButton;
    private JTextField txtResultado;
    private JTextField txtNumeroTicket;
    private JTextField txtNumeroIteracion;
    private JTextField txtNumeroRequerimiento;
    private JTextField txtNumeroEscenario;
    private JButton btnBuscarTicket;
    private JButton btnBuscarIteracion;
    private JButton btnBuscarRequerimiento;
    private JButton btnBuscarEscenario;
    private JButton btnCrearPrueba;
    private JTable tbMostrarTicket;
    private JButton mostrarTodosLosTicketButton;
    private JButton mostrarTodasLasPruebasButton;
    private JTable tbMostrarPruebas;
    private JButton cargarDatosButton;
    private JTable tbCargarDatos;
    private JTextField txtTicketIdBuscar;
    private JButton btnBuscarTicketIteracion;
    private Sistema sistema = new Sistema();
    private List<Escenario> listadoEscenario = null;
    private DefaultTableModel modelEscenario;
    private DefaultTableModel modelTicketIteracion;
    private DefaultTableModel modelTicketPruebas;
    private DefaultTableModel ModelTicketPruebasCargarDatos;

    public aplicacionTicket() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        modelEscenario = new DefaultTableModel();
        modelEscenario.addColumn("Descripcion");
        modelEscenario.addColumn("Resultado esperado");
        tbEscenarios.setModel(modelEscenario);

        modelTicketIteracion = new DefaultTableModel();
        modelTicketIteracion.addColumn("Numero Ticket");
        modelTicketIteracion.addColumn("Memorandum");
        modelTicketIteracion.addColumn("Numero iteracion");
        tbMostrarTicket.setModel(modelTicketIteracion);

        modelTicketPruebas = new DefaultTableModel();
        modelTicketPruebas.addColumn("Numero ticket");
        modelTicketPruebas.addColumn("Analista desarrollador");
        modelTicketPruebas.addColumn("Requerimiento");
        modelTicketPruebas.addColumn("Version");
        modelTicketPruebas.addColumn("Resultado");
        tbMostrarPruebas.setModel(modelTicketPruebas);

        ModelTicketPruebasCargarDatos = new DefaultTableModel();
        ModelTicketPruebasCargarDatos.addColumn("Numero ticket");
        ModelTicketPruebasCargarDatos.addColumn("Analista desarrollador");
        ModelTicketPruebasCargarDatos.addColumn("Requerimiento");
        ModelTicketPruebasCargarDatos.addColumn("Version");
        ModelTicketPruebasCargarDatos.addColumn("Resultado");
        tbCargarDatos.setModel(ModelTicketPruebasCargarDatos);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        btnCrearTicket.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Ticket ticket = new Ticket();
                ticket.setMemorandum(txtMemorandun.getText());
                ticket.setDescripcion(txtDescripcion.getText());
                ticket.setTicketId(Integer.parseInt(txtTicketId.getText()));
                sistema.AgregarTicket(ticket);
                JOptionPane.showMessageDialog(getParent(),"Se creo con exito el ticket: "+ ticket.getTicketId());
            }
        });

        btnCrearIteracion.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Iteracion iteracion = new Iteracion();
                iteracion.setAnalisitaQA(txtAnlistaQA.getText());
                iteracion.setAnalistaDesa(txtAnalisitaDessa.getText());
                iteracion.setAnalistaPDI(txtAnliastaDPI.getText());
                iteracion.setAnalistaNormativo(txtAnlistaNorma.getText());
                sistema.AgregarIteraccionTicket(Integer.parseInt(txtTicketIdBuscar.getText()), iteracion);
                JOptionPane.showMessageDialog(getParent(), "Se ingreso correctamente la iteracion");
            }
        });

        btnBuscarTicketIteracion.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String menu = sistema.MostrarTicketsVisual();
                String numeroTicket2 = JOptionPane.showInputDialog(menu);
                txtTicketIdBuscar.setText(numeroTicket2);
            }
        });

        crearRequerimientoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Requerimiento requerimiento = new Requerimiento();
                requerimiento.setNombreRquerimiento(txtNombreRequerimiento.getText());
                requerimiento.setVersion(txtVersion.getText());
                requerimiento.setListadoEscenarios(listadoEscenario);
                sistema.AgregarRequerimiento(requerimiento);
                JOptionPane.showMessageDialog(getParent(), "Se creo exitosamente el requerimiento");
            }
        });

        btnAgregarEscenario.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(listadoEscenario == null)
                {
                    listadoEscenario = new ArrayList<>();
                }
                Escenario escenario = new Escenario();
                escenario.setDescripcion(txtDescripcionEscenario.getText());
                escenario.setResultadoEsperado(txtResultadoEscenario.getText());
                listadoEscenario.add(escenario);
                modelEscenario.addRow(new Object[]{escenario.getDescripcion(), escenario.getResultadoEsperado()});
            }
        });

        btnBuscarTicket.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String menu = sistema.MostrarTicketsVisual();
                String numeroTicket2 = JOptionPane.showInputDialog(menu);
                txtNumeroTicket.setText(numeroTicket2);
            }
        });

        btnBuscarIteracion.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(txtNumeroTicket.getText() == " ")
                {
                    JOptionPane.showMessageDialog(getParent(), "Debe de buscar un ticket para asignar una iteracion");
                }
                else
                {
                    int valorIndice = Integer.parseInt(txtNumeroTicket.getText())-1;
                    String menu = sistema.MostrarIteracionesVisual(valorIndice);
                    String numeroIteracion = JOptionPane.showInputDialog(menu);
                    txtNumeroIteracion.setText(numeroIteracion);
                }
            }
        });

        btnBuscarRequerimiento.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String menu = sistema.MostrarRequerimientosVisual();
                String numeroRequerimiento = JOptionPane.showInputDialog(menu);
                txtNumeroRequerimiento.setText(numeroRequerimiento);
            }
        });

        btnBuscarEscenario.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(txtNumeroRequerimiento.getText() == "")
                {

                }
                else
                {
                    int valorIndice = Integer.parseInt(txtNumeroRequerimiento.getText()) - 1;
                    String menu = sistema.MostrarEscenariosVisual(valorIndice);
                    String numeroEscenario = JOptionPane.showInputDialog(menu);
                    txtNumeroEscenario.setText((numeroEscenario));
                }
            }
        });
        btnCrearPrueba.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String resultado = txtResultado.getText();
                int indiceTicket = Integer.parseInt(txtNumeroTicket.getText()) - 1;
                int indiceIteracion = Integer.parseInt(txtNumeroIteracion.getText()) - 1;
                int indiceRequerimiento = Integer.parseInt(txtNumeroRequerimiento.getText()) - 1;
                int indiceEscenario = Integer.parseInt(txtNumeroEscenario.getText()) - 1;
                Prueba prueba = new Prueba(resultado, indiceTicket, indiceIteracion, indiceRequerimiento, indiceEscenario);
                sistema.AgregarPruebaTicket(prueba);
                JOptionPane.showMessageDialog(getParent(), "Se asigno correctamente la prueba");
            }
        });

        mostrarTodosLosTicketButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<Ticket> listadoTicket =  sistema.getListadoTicket();
                for (Ticket t: listadoTicket
                     ) {
                    int numeroTicket = t.getTicketId();
                    String memorandum = t.getMemorandum();
                    int contar = 1;
                    for (Iteracion i: t.getListadoIteraciones()
                         ) {
                        modelTicketIteracion.addRow(new Object[]{numeroTicket, memorandum, contar});
                        contar++;
                    }
                }
            }
        });
        mostrarTodasLasPruebasButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<Ticket> listadoTicket = sistema.getListadoTicket();
                for (Ticket t:listadoTicket
                        ) {
                    int numeroTicket = t.getTicketId();
                    String memorandum = t.getMemorandum();
                    if(!t.getListadoPruebas().isEmpty()){
                        for (Prueba p: t.getListadoPruebas()
                                ) {
                            Ticket ticket = listadoTicket.get(p.getIndiceTicket());
                            Iteracion iteracion = ticket.getListadoIteraciones().get(p.getIndiceIteracion());
                            List<Requerimiento> listadoRequerimiento = sistema.getListadoRequerimientos();
                            Requerimiento requerimiento = listadoRequerimiento.get(p.getIndiceRequerimiento());
                            Escenario escenario = requerimiento.getListadoEscenarios().get(p.getIndiceEscenario());

                            modelTicketPruebas.addRow(new Object[]{ticket.getTicketId(), iteracion.getAnalistaDesa(), requerimiento.getNombreRquerimiento(), requerimiento.getVersion(),p.getResultado()});
                        }
                    }
                }
            }
        });
        cargarDatosButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                CargarDatos cargarDatos = new CargarDatos();
                sistema.setListadoRequerimientos(cargarDatos.getListadoRequerimiento());
                sistema.setListadoTicket(cargarDatos.getListadoTicke());

                List<Ticket> listadoTicket = sistema.getListadoTicket();
                for (Ticket t:listadoTicket
                        ) {
                    int numeroTicket = t.getTicketId();
                    String memorandum = t.getMemorandum();
                    if(!t.getListadoPruebas().isEmpty()){
                        for (Prueba p: t.getListadoPruebas()
                                ) {
                            Ticket ticket = listadoTicket.get(p.getIndiceTicket());
                            Iteracion iteracion = ticket.getListadoIteraciones().get(p.getIndiceIteracion());
                            List<Requerimiento> listadoRequerimiento = sistema.getListadoRequerimientos();
                            Requerimiento requerimiento = listadoRequerimiento.get(p.getIndiceRequerimiento());
                            Escenario escenario = requerimiento.getListadoEscenarios().get(p.getIndiceEscenario());

                            ModelTicketPruebasCargarDatos.addRow(new Object[]{ticket.getTicketId(), iteracion.getAnalistaDesa(), requerimiento.getNombreRquerimiento(), requerimiento.getVersion(),p.getResultado()});
                        }
                    }
                }
            }
        });
    }

    private void onOK() {
        // add your code here
        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public static void main(String[] args) {
        aplicacionTicket dialog = new aplicacionTicket();
        dialog.pack();
        //dialog.setSize(dialog.getToolkit().getScreenSize());
        //dialog.setSize(500, 500);
        dialog.setLocationRelativeTo(null);
        dialog.setVisible(true);
        System.exit(0);
    }
}
